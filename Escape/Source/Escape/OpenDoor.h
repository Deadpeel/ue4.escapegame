// Copyright Angel

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	float TotalMassOfActors() const;
	void FindAudioByComponent();
	void FindPressurePlate();


private:
	UPROPERTY(EditAnywhere)
	bool TheDoorClosing = false;
	bool DoorHasBeenOpened = false;

	bool OpenFeedback = false;  
	bool CloseFeedback = true;  

	float InitialYaw;
	float CurrentYaw;

	UPROPERTY(EditAnywhere)
	float MassRequired;
	
	UPROPERTY(EditAnywhere)
	float TargetYaw = 90.0f;

	float DoorLastOpened = 0.f;
	
	UPROPERTY(EditAnywhere)
	float DoorClosedDelay = 2.f;

	UPROPERTY(EditAnywhere)
	float DoorOpenSpeed = 1.f;
	UPROPERTY(EditAnywhere)
	float DoorCloseSpeed = 1.f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate = nullptr;

	UPROPERTY()
		UAudioComponent* AudioComponent = nullptr;
};
